describe('Transactions tests', () => {    

	// a user who is in transactions screen
	beforeEach(() => {
		cy.visit('http://localhost:4200');

		sessionStorage.clear();

		cy.get('[formControlName="name"]')
			.type('user')
			.should('have.value', 'user');

		cy.get('[formControlName="password"]')
			.type('pass')
			.should('have.value', 'pass');

		cy.contains('Login').click();
	});

    // Then the system displays transactions sorted descending by date
    it('Transaction list sorted descending by date', () => {
      let isOrdered;
			cy.get('tbody tr td:nth-child(2)')
				.each(($el, $index, $elements) => {
					if ($index > 0) {  
						const previousDate = $elements[$index - 1].innerHTML.split('/').reverse().join('-');
						const date = $el.html().split('/').reverse().join('-');
						isOrdered = new Date(date) <= new Date(previousDate);
					
						expect(isOrdered).to.equal(true);
					}
				});
    });

    //  there are no transactions
    it('Transaction empty list - The system displays an empty notification message', () => {
			cy.get('[formControlName="filterInput"]')
				.type('nocoincideconnada');

			cy.get('#filterButton').click();

			// the system displays an empty notification message
			cy.get('tbody tr').should('have.length', 1);
			cy.get('tbody tr td').should('have.length', 1);
			cy.get('tbody tr td h3').should('have.length', 1);
    });
    
    // he tries to sort by date
    it('Transaction sorted list - He tries to sort by date', () => {
      cy.get('#orderButton').click();
      cy.wait(3000);

			let isOrdered;
			cy.get('tbody tr td:nth-child(2)')
				.each(($el, $index, $elements) => {
					if ($index > 0) {
						const previousDate = $elements[$index - 1].innerHTML.split('/').reverse().join('-');
						const date = $el.html().split('/').reverse().join('-');
						isOrdered = new Date(date) >= new Date(previousDate);

						expect(isOrdered).to.equal(true);
					}
				});
    });
    
    // he tries to search a transaction
    it('Transaction filtered list - The system displays matching transactions', () => {
			const needle = 'Lorem';

			cy.get('[formControlName="filterInput"]')
				.type(needle);
			cy.get('#filterButton').click();

			cy.wait(3000);

			// the system displays matching transactions
			cy.get('tbody tr td:nth-child(5)')
				.each(($el, $index, $elements) => {
					const text = $el.text();
					const haveNeedle = text.indexOf(needle) > -1 || text.indexOf(needle.toLocaleLowerCase()) > -1;
					
					expect(haveNeedle).to.equal(true);
				});

			expect(true).to.equal(true);
    });
});