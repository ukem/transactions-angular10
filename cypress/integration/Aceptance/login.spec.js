describe('Login Tests', () => {

	beforeEach(() => {
		cy.visit('http://localhost:4200');
		sessionStorage.clear();
	});
	

	it('Login success - a user who already has an account', () => {	
		// When he tries to login with valid credentials 
		cy.get('[formControlName="name"]')
				.type('user')
				.should('have.value', 'user');
		
		cy.get('[formControlName="password"]')
				.type('pass')
				.should('have.value', 'pass');
		
		cy.contains('Login').click();

		// Then the system redirects to transaction list
		cy.url().should('include', '/page/transactions');
	});

	it('Login fails - a user who already has an account', () => {	
		// When he tries to login with invalid credentials
		cy.get('[formControlName="name"]')
				.type('another user')
				.should('have.value', 'another user');
		
		cy.get('[formControlName="password"]')
				.type('another pass')
				.should('have.value', 'another pass');
		
		cy.contains('Login').click();
		
		// the system displays an error
		cy.get('.text-danger')
				.contains('Invalid credentials');
	});

	it('Security redirect - a user who is not logged in', () => {
		// When he tries to access transactions screen
		cy.visit('http://localhost:4200/page/transactions');

		// Then the system redirects to login page
		cy.url().should('include', '/login');
	});
})
