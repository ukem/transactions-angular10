import { Component, OnInit, forwardRef } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';

import { TransactionsService } from '../../services/transactions.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  orderTitles = ['ORDER ASC', 'ORDER DESC'];
  orderTitle = 0;

  filterForm = this.fb.group({
    filterInput: ['']
  });

  constructor(
    private fb: FormBuilder,
    private transactionsService: TransactionsService
  ) { }

  ngOnInit(): void {
    this.filterForm.valueChanges.subscribe(value => {
      if (value.filterInput === '') {
        this.filterList(false);
      }
    });
  }

  filterList(event) {
    if(event) {
      event.preventDefault();
      event.stopPropagation();
    }

    const description = this.filterForm.get('filterInput');
    this.transactionsService.setFilter(description.value);
  }

  orderList(event) {
    event.preventDefault();
    event.stopPropagation();

    this.transactionsService.setSort(['asc', 'desc'][this.orderTitle]);
    this.orderTitle = Math.abs(--this.orderTitle % 2);
  }
}
