import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import localeEs from '@angular/common/locales/es';
import { registerLocaleData } from '@angular/common';

import { AppRoutingModule } from './app-routing.module';
import { PagesModule } from './pages/pages.module';
import { SharedModule } from './shared/shared.module';
import { AuthModule } from './auth/auth.module';

import { AppComponent } from './app.component';
import { NotfoundComponent } from './pages/notfound/notfound.component';

import { PagesComponent } from './pages/pages.component';

registerLocaleData(localeEs, 'es');

@NgModule({
  declarations: [
    AppComponent,
    NotfoundComponent,
    PagesComponent
  ],
  imports: [
		BrowserModule,
		HttpClientModule,
		AppRoutingModule,
		PagesModule,
		SharedModule,
		AuthModule
  ],
  providers: [{
		provide: LOCALE_ID,
		useValue: 'es'
	}],
  bootstrap: [AppComponent]
})
export class AppModule { }
