import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
	providedIn: 'root'
})
export class TransactionsService {

	SERVER = 'https://us-central1-code-challenge-e9f47.cloudfunctions.net/app';
	HEADERS = new HttpHeaders({
		'Content-Type': 'application/json',
		Authorization: 'Bearer ' + JSON.parse(sessionStorage.getItem('token'))
	});

	private transactions: BehaviorSubject<any>;

	public transactions$: Observable<any>;

	sort = 'desc';
	filter = '';

	constructor(
		private http: HttpClient
	) {
		this.transactions = new BehaviorSubject(this.getTransactions());
		this.transactions$ = this.transactions.asObservable();
	}

	get params(): string {
		let queryParams = '';
		const params = [];

		if (this.sort) {
			params.push(`sort=${this.sort}`);	
		}

		if (this.filter) {
			params.push(`description=${this.filter}`);
		}

		if (params.length > 0) {
			queryParams = `?${params.length > 1 ? params.join('&') : params[0]}`;
		}

		return queryParams;
	}

	getTransactions(): void {
		const options = {
			headers: this.HEADERS
		};

		const url = `${this.SERVER}/transactions${this.params}`;

		this.http.get(url, options).subscribe(data => {
			this.transactions.next(data);
		});
	}

	setSort(sortBy): void {
		this.sort = sortBy;
		this.getTransactions();
	}

	setFilter(filterBy): void {
		this.filter = filterBy;
		this.getTransactions();
	}
}
