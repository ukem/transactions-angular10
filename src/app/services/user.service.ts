import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserService {

	SERVER = 'https://us-central1-code-challenge-e9f47.cloudfunctions.net/app';
	HEADERS = new HttpHeaders({
		'Content-Type': 'application/json'
	});

	constructor(
		private http: HttpClient
	) {}

	getUserToken(username, password): Promise<object> {
		const options = {
			headers: this.HEADERS
		};

		const body = { username, password };

		return this.http.post(`${this.SERVER}/token`, body, options).toPromise();
	}
}
