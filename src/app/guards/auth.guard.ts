import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from '@angular/router';

@Injectable({
	providedIn: 'root'
})
export class AuthGuard implements CanActivate {
	constructor(
		private router: Router
	) {}

	readonly canActivate = (
		next: ActivatedRouteSnapshot,
		state: RouterStateSnapshot
	) => {
		const token = sessionStorage.getItem('token');

		return Boolean(token) || this.router.parseUrl('/login');
	}
}
