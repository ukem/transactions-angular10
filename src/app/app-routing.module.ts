import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PagesRoutingModule } from './pages/pages.routing';

import { LoginComponent } from './auth/login/login.component';
import { NotfoundComponent } from './pages/notfound/notfound.component';

const routes: Routes = [
	{ path: 'login', component: LoginComponent },
	{ path: '', redirectTo: 'page/transactions', pathMatch: 'full' },
	{ path: '**', component: NotfoundComponent }
];

@NgModule({
	imports: [
		RouterModule.forRoot(routes),
		PagesRoutingModule
	],
	exports: [RouterModule]
})
export class AppRoutingModule { }
