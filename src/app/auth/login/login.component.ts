import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import { UserService } from '../../services/user.service';

@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

	invalidCredentials = false;

	loginForm = this.fb.group({
		name: ['',  [Validators.required]],
		password: ['', [Validators.required]]
	});

	constructor(
		private userService: UserService,
		private fb: FormBuilder,
		private router: Router
	) { }

	ngOnInit(): void {
		sessionStorage.removeItem('token');
	}

	loginUser(event): void {
		event.preventDefault();
		event.stopPropagation();

		const name = this.loginForm.get('name');
		const pass = this.loginForm.get('password');

		if (name.valid && pass.valid) {
			this.userService.getUserToken(name.value, pass.value)
				.then((token: {accessToken: string}) => {
					this.invalidCredentials = false;
					sessionStorage.setItem('token', JSON.stringify(token.accessToken));
					this.router.navigate(['page/transactions']);
				})
				.catch(error => {
					console.error('ERROR: ', error);
					this.invalidCredentials = true;
				});
			}
	}
}
