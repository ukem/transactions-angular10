import { Component, OnInit } from '@angular/core';

import { TransactionsService } from '../../services/transactions.service';
import { faSortAmountUp, faSortAmountDown } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-transactions',
  templateUrl: './transactions.component.html',
  styleUrls: ['./transactions.component.scss']
})
export class TransactionsComponent implements OnInit {

	sortUpIcon = faSortAmountUp;
	sortDownIcon = faSortAmountDown;

	transactions: any = [];

	constructor(
		private transactionsService: TransactionsService
	) { }

	ngOnInit(): void {
		this.transactionsService.transactions$.subscribe(transactions => {
			this.transactions = transactions;
		});
	}
}
