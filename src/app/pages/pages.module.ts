import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { TransactionsComponent } from './transactions/transactions.component';
import { RowComponent } from './transactions/components/row/row.component';

@NgModule({
  declarations: [
		TransactionsComponent,
		RowComponent
	],
	exports: [
		TransactionsComponent
	],
  imports: [
		CommonModule,
		FontAwesomeModule
  ]
})
export class PagesModule { }
