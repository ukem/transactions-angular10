import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

import { AuthGuard } from '../guards/auth.guard';

import { PagesComponent } from './pages.component';
import { TransactionsComponent } from './transactions/transactions.component';



const routes: Routes = [
	{
		path: 'page',
		component: PagesComponent,
		canActivate: [ AuthGuard ],
		children: [
			{ path: 'transactions', component: TransactionsComponent }
		],
	}
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class PagesRoutingModule {}
